package Tests;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import org.junit.jupiter.api.Test;
import Controllers.TrancaController;
import io.javalin.http.Context;

public class TrancaControllerTest {
	private Context ctx = mock(Context.class);
	
	@Test
	public void fetchAllTrancasTest() {
		TrancaController.fetchTranca(ctx);
		verify(ctx).status(200);
	}
	
	@Test
	public void fetchTrancaTest() {
		when(ctx.queryParam("id")).thenReturn("01");
		TrancaController.fetchTranca(ctx);
		verify(ctx).status(200);
	}
	
	@Test
	public void deleteTrancaTest() {
		when(ctx.queryParam("id")).thenReturn("01");
		TrancaController.deleteTranca(ctx);
		verify(ctx).status(200);
	}
	
	@Test
	public void adicionarTrancaTest() {
		when(ctx.queryParam("id")).thenReturn("01");
		TrancaController.adicionarTranca(ctx);
		verify(ctx).status(200);
	}
	
	@Test
	public void alterarTrancaTest() {
		when(ctx.queryParam("id")).thenReturn("01");
		TrancaController.alterarTrancaStatus(ctx);
		verify(ctx).status(200);
	}
}
