package Tests;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import org.junit.jupiter.api.Test;
import Controllers.TotemController;
import io.javalin.http.Context;

public class TotemControllerTest {
	private Context ctx = mock(Context.class);
	
	@Test
	public void fetchAllTotemsTest() {
		TotemController.fetchTotem(ctx);
		verify(ctx).status(200);
	}
	
	@Test
	public void fetchTotemTest() {
		when(ctx.queryParam("id")).thenReturn("01");
		TotemController.fetchTotem(ctx);
		verify(ctx).status(200);
	}
	
	@Test
	public void deleteTotemTest() {
		when(ctx.queryParam("id")).thenReturn("01");
		TotemController.deleteTotem(ctx);
		verify(ctx).status(200);
	}
	
	@Test
	public void adicionarTotemTest() {
		when(ctx.queryParam("id")).thenReturn("01");
		TotemController.adicionarTotem(ctx);
		verify(ctx).status(200);
	}
}
