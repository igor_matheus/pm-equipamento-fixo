package App;
import io.javalin.Javalin;
import static io.javalin.apibuilder.ApiBuilder.*;

import Controllers.TotemController;
import Controllers.TrancaController;

public class ApplicationMain {

	public static void main(String[] args) {
		
    	 @SuppressWarnings("unused")
		Javalin app = Javalin.create(config -> {
             config.defaultContentType = "application/json";
         }).routes(() -> {
         	path("", () -> {
         		get(ctx -> ctx.result("Hello World"));
         	});
         	path("/tranca", () -> {
         		get(TrancaController::fetchTranca);
         		post(TrancaController::adicionarTranca);
         		delete(TrancaController::deleteTranca);
         	});
         	path("/totem", () -> {
         		get(TotemController::fetchTotem);
         		post(TotemController::adicionarTotem);
         		delete(TotemController::deleteTotem);
         	});
         	path("/statusTranca", () -> {
         		get(TrancaController::fetchTrancaStatus);
         		post(TrancaController::alterarTrancaStatus);
         	});
         }).start(7780);
	}
}